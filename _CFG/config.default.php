<?php
return array(
    'MANAGE_SCRIPT' => 'manage.php',
    'ADMIN_SCRIPT' => 'admin.php',
    'AUTO_SEND_REPORTLOG' => true,
    'SHOW_PAGE_TRACE' => false,
    'DB_DEBUG' => DB_DEBUG ? true : false,
    'TMPL_PARSE_STRING' => array(
        '__ASSERTS__' => __ROOT__ . '/asserts',
        '__DATA__' => __ROOT__ . '/data',
        '__STATIC_ROOT__' => __ROOT__,
        '__JS_SUFFIX__' => (APP_DEBUG ? '.src.js' : '.js'),
        '__CSS_SUFFIX__' => (APP_DEBUG ? '.src.css' : '.css')
    ),
    'APP_SUB_DOMAIN_DEPLOY' => 1,
    'APP_SUB_DOMAIN_RULES' => array(
        //'domain1.com' => 'Home',
        //'domain2.com' => 'Home'
    ),
    'UPLOAD_TEMP_DIR' => DATA_PATH,
    'USER_UPLOAD' => array(
        'IMAGE_MAX_SIZE' => 10 * 1024 * 1024,
        'IMAGE_ALLOW_EXT' => array(
            '.png',
            '.jpg',
            '.gif'
        ),
        'SCRAWL_MAX_SIZE' => 10 * 1024 * 1024,
        'CATCHER_MAX_SIZE' => 10 * 1024 * 1024,
        'CATCHER_ALLOW_EXT' => array(
            '.png',
            '.jpg',
            '.gif'
        ),
        'VIDEO_MAX_SIZE' => 100 * 1024 * 1024,
        'VIDEO_ALLOW_EXT' => array(
            '.flv',
            '.mp4',
        ),
        'FILE_MAX_SIZE' => 20 * 1024 * 1024,
        'FILE_ALLOW_EXT' => array(
            '.mp3',
        ),
        'IMAGE_LIST_EXT' => array(
            '.png',
            '.jpg',
            '.gif',
        ),
        'FILE_LIST_EXT' => array(
            '.png',
            '.jpg',
            '.gif',
            '.flv',
            '.mp4',
            '.mp3',
        )
    ),
    'ADMIN_UPLOAD' => array(
        'LARGE_FILE_MAX_SIZE' => 2 * 1024 * 1024 * 1024,
        'LARGE_FILE_ALLOW_EXT' => array(
            '.flv',
            '.jpg'
        ),
        'IMAGE_MAX_SIZE' => 10 * 1024 * 1024,
        'IMAGE_ALLOW_EXT' => array(
            '.png',
            '.jpg',
            '.gif'
        ),
        'SCRAWL_MAX_SIZE' => 10 * 1024 * 1024,
        'CATCHER_MAX_SIZE' => 10 * 1024 * 1024,
        'CATCHER_ALLOW_EXT' => array(
            '.png',
            '.jpg',
            '.gif'
        ),
        'VIDEO_MAX_SIZE' => 100 * 1024 * 1024,
        'VIDEO_ALLOW_EXT' => array(
            '.flv',
            '.mp4',
        ),
        'FILE_MAX_SIZE' => 20 * 1024 * 1024,
        'FILE_ALLOW_EXT' => array(
            '.mp3',
        ),
        'IMAGE_LIST_EXT' => array(
            '.png',
            '.jpg',
            '.gif',
        ),
        'FILE_LIST_EXT' => array(
            '.png',
            '.jpg',
            '.gif',
            '.flv',
            '.mp4',
            '.mp3',
        )
    )
);